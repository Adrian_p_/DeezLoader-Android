package com.dt3264.deezloader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class BrowserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        vcm.github.webkit.proview.ProWebView webView = findViewById(R.id.webView);
        webView.clearCache(true);
        webView.clearCookies();
        webView.clearHistory();
        webView.clearDatabase();
        webView.clearStorage();
        webView.loadUrl("http://localhost:1730");
    }
}
